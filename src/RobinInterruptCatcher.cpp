/************************************************************************/
/*                                                                      */
/* File             : RobinInterruptCatcher.cpp				*/
/*                                                                      */
/* Author           : M.Joos, CERN                                      */
/*                                                                      */
/* A singleton class for catching interrupts from Robin cards		*/
/*									*/
/***** C 2010 Ecosoft - Made from at least 80% recycled source code *****/

#define HOST   //for robin.h

#include <signal.h>
#include <sys/types.h>
#include "DFDebug/DFDebug.h"
#include "ROSUtilities/ROSErrorReporting.h"
#include "ROSMemoryPool/MemoryPool_malloc.h"
#include "ROSEventFragment/ROBFragment.h"
//#include "robin_ppc/robin.h"
#include "ROSInterruptScheduler/InterruptSchedulerException.h"
#include "ROSInterruptScheduler/RobinInterruptCatcher.h"

#define KILL_SIGNAL SIGUSR2

using namespace ROS;


//Static variables
DFFastMutex *RobinInterruptCatcher::s_mutex = DFFastMutex::Create();
RobinInterruptCatcher *RobinInterruptCatcher::s_uniqueInstance = 0;
static struct sigaction m_saint, m_saold;          // For the handling of the signal that we need to stop the thread


//used by RobinInterruptCatcher (see below)
/***********************************************************************/
static void KILL_SIGNALHandler(int sig, siginfo_t *info, void * /*data*/) 
/***********************************************************************/
{  
  DEBUG_TEXT(DFDB_ROBINIRQ, 10, "RobinInterruptCatcher::KILL_SIGNALHandler, signal # = " << sig);

  if(info == NULL) 
  {
    std::cout << "signal: " << sig << std::endl;
    return;
  }

  std::cout << "signal: " << sig << ", pid: " << info->si_pid << ", uid: " << info->si_uid << std::endl;
}


//Singleton pattern: only one instance is created
//Called by ROSCore/IOManager.cpp 
/******************************************************/
RobinInterruptCatcher *RobinInterruptCatcher::Instance() 
/******************************************************/
{
  s_mutex->lock();
  if (s_uniqueInstance == 0) 
    s_uniqueInstance = new RobinInterruptCatcher;

  // We will start a thread. Therefore we have to install a signal handler for stopping it
  std::cout << "RobinInterruptCatcher: Installing handler for signal " << KILL_SIGNAL << std::endl;
  memset(&m_saint, '\0', sizeof (struct sigaction));
  m_saint.sa_sigaction = &KILL_SIGNALHandler;
  m_saint.sa_flags |= SA_SIGINFO;
  sigemptyset(&m_saint.sa_mask);
  if(sigaction(KILL_SIGNAL, &m_saint, &m_saold))
  {
    perror("sigaction");
    DEBUG_TEXT(DFDB_ROBINIRQ, 5, "RobinInterruptCatcher::Instance: Error from sigaction");
  } 

  s_mutex->unlock();  
  return s_uniqueInstance;
}


//The constructor should never get called directly. As we want a singleton all accesses have to go via Instance()
/****************************************************************************************************************************/
RobinInterruptCatcher::RobinInterruptCatcher(void) : ROSHandler("RobinInterruptCatcher"), m_threadStarted(false), m_nrobins(0)
/****************************************************************************************************************************/
{
  DEBUG_TEXT(DFDB_ROBINIRQ, 15, "RobinInterruptCatcher::constructor: Entered");

  m_statistics.interruptsReceived = 0;
}


/*********************************************/
RobinInterruptCatcher::~RobinInterruptCatcher() noexcept
/*********************************************/
{
  DEBUG_TEXT(DFDB_ROBINIRQ, 15, "RobinInterruptCatcher::destructor: Entered");
      
  //Now the signal handler is not required any more
  if (sigaction(KILL_SIGNAL, &m_saold, (struct sigaction *)NULL))
  {
    perror("sigaction");
    DEBUG_TEXT(DFDB_ROBINIRQ, 5, "RobinInterruptCatcher::destructor: Error from sigaction");
  }  

 s_uniqueInstance = 0;
}


//Called from ROSCore/IOManager.cpp
/***********************************************************************/
void RobinInterruptCatcher::setup(DFCountedPointer<Config> configuration)
/***********************************************************************/
{  
  DEBUG_TEXT(DFDB_ROBINIRQ, 15, "RobinInterruptCatcher::setup: Entered. Almost empty function");

  m_configuration.push_back(configuration);
}


/****************************************************************************/
void RobinInterruptCatcher::deregister(DFCountedPointer<Config> configuration)
/****************************************************************************/
{
  DEBUG_TEXT(DFDB_ROBINIRQ, 15, "RobinInterruptCatcher::deregister: Entered ");
  for (std::vector< DFCountedPointer<Config> >::iterator iter = m_configuration.begin(); iter != m_configuration.end(); iter++) 
  {
    if ((*iter) == configuration) 
    {
      m_configuration.erase(iter);
      break;
    }
  }
}


//Called by ROSCore/IOManager.cpp
/*****************************************/
void RobinInterruptCatcher::configure(const daq::rc::TransitionCmd&)
/*****************************************/
{
  DEBUG_TEXT(DFDB_ROBINIRQ, 15, "RobinInterruptCatcher::configure: Entered ");

  m_nrobins = m_configuration.size();
  DEBUG_TEXT(DFDB_ROBINIRQ, 20, "RobinInterruptCatcher::configure: m_nrobins = " << m_nrobins);
  
  if (m_nrobins == 0)		// do not start the thread if there are no Robins 
    return;
  
  m_themaxevsize = 0;

  for(int card = 0; card < m_nrobins; card++)
  {
    u_int pagesize   = m_configuration[card]->getInt("Pagesize");
    u_int maxrxpages = m_configuration[card]->getInt("MaxRxPages");
    u_int physaddr   = m_configuration[card]->getInt("PhysicalAddress");
    m_cardmap[physaddr] = card;
    
    DEBUG_TEXT(DFDB_ROBINIRQ, 20, "RobinInterruptCatcher::configure: card = " << card << ": pagesize = " << pagesize << ", physaddr = " << physaddr << ", maxrxpages = " << maxrxpages);

    u_int maxevsize = maxrxpages * pagesize * 4;  //In bytes
    DEBUG_TEXT(DFDB_ROBINIRQ, 20, "RobinInterruptCatcher::configure: card = " << card << ": maxevsize = " << maxevsize);
    
    if (maxevsize > m_themaxevsize)
    {
      m_themaxevsize = maxevsize;
      DEBUG_TEXT(DFDB_ROBINIRQ, 20, "RobinInterruptCatcher::configure: card = " << card << ": New m_themaxevsize = " << m_themaxevsize);
    }
    
    DEBUG_TEXT(DFDB_ROBINIRQ, 20, "RobinInterruptCatcher::configure: Creating Robin object for card " << physaddr);
    m_robin[card] = new Robin(physaddr, 100);
    m_robin[card]->open();
    
    //In principle the pages in jail have the same size as the pages in the regular memory. This page size is a configuration
    //parameter and here we get it from m_configuration->getInt("PageSize", card). 
    //However the function getRejectedPage (Rol.cpp) which we will call from get_inmate_event does not know how big a
    //page actuall is. Therefore it allocates space for the biggest possible page size which is 8 KB (plus a bit for headers)
    //As a consequence we have to over-dimension the miscMem here as well.
    //The minimum would be: sizeof(RespMsg) + sizeof(RejectedEntry) + enum_mgmtMaxPageSize * sizeof(int);
    //For convenience we are generously adding 1K to the MaxPageSize
    u_int miscMem = enum_mgmtMaxPageSize * sizeof(int) + 0x1024;  
    m_robin[card]->reserveMsgResources(1, 0x20, miscMem, maxevsize); 

    for (int rol = 0; rol < 3; rol++)
      m_rol[card * 3 + rol] = new Rol(*m_robin[card], rol);
  
    // Create a memoryPool structure around the event buffer
    m_memoryPool[card] = new WrapperMemoryPool(1, maxevsize, m_robin[card]->getVirtEventBase(), m_robin[card]->getPhysEventBase());  
  } 
  
  //So far "m_themaxevsize" is the maximum size of a ROD fragment. Now we add some extra space for the ROB header and trailer
  m_themaxevsize += (sizeof(ROBFragment::ROBHeader) + sizeof(ROBFragment::ROBTrailer));
  DEBUG_TEXT(DFDB_ROBINIRQ, 20, "RobinInterruptCatcher::configure: Final m_themaxevsize = " << m_themaxevsize);

  //Create a pool for the storage of corrupted fragments (we just need one page)
  m_dataPool = new MemoryPool_malloc(1, m_themaxevsize);
  DEBUG_TEXT(DFDB_ROBINIRQ, 20, "RobinInterruptCatcher::configure: Created pool with 1 pages of size " << m_themaxevsize);
  m_mem_buff = new Buffer(m_dataPool);

  m_debugOut = DataOut::debug();
  if (m_debugOut == 0) 
  {
    DEBUG_TEXT(DFDB_ROBINIRQ, 5, "RobinInterruptCatcher::configure: m_debugOut is NULL");
    CREATE_ROS_EXCEPTION(ex2, InterruptSchedulerException, ROSINT_DATAOUT, "Therefore the interrupt handler thread will not be started and corrupted fragments will not be sent to the monitoring system.");
    ers::warning(ex2);
    return; //Do not start the thread
  } 

  DEBUG_TEXT(DFDB_ROBINIRQ, 20, "RobinInterruptCatcher::configure: going to start the thread");
  startExecution();
  m_threadStarted = true;
  DEBUG_TEXT(DFDB_ROBINIRQ, 20, "RobinInterruptCatcher::configure: Interrupt Catcher started");
  DEBUG_TEXT(DFDB_ROBINIRQ, 15, "RobinInterruptCatcher::configure: Done");
}


//Called by ROSCore/IOManager.cpp
/*******************************************/
void RobinInterruptCatcher::unconfigure(const daq::rc::TransitionCmd&)
/*******************************************/
{
  DEBUG_TEXT(DFDB_ROBINIRQ, 15, "RobinInterruptCatcher::unconfigure: Entered.");

  //MJ: if I use a blocking wait for the detection of the interrupt it may not be possible to stop the thread with a signal. To be tested

  if (m_threadStarted) 
  {
    //Stop the thread. Just send a signal and the thread should stop itself
    //remember that this code executes as part of the 'main' thread - NOT the interrupt catcher thread.
    DEBUG_TEXT(DFDB_ROBINIRQ, 20, "RobinInterruptCatcher::unconfigure: stopping interrupt catcher thread");

    pthread_t icId = s_uniqueInstance->specificId();  //MJ: This seems to be the thread ID
    DEBUG_TEXT(DFDB_ROBINIRQ, 20, "RobinInterruptCatcher::unconfigure: send KILL_SIGNAL to tid = 0x" << HEX(icId));

    int status = pthread_kill(icId, KILL_SIGNAL);
    if (status)
    {    
      DEBUG_TEXT(DFDB_ROBINIRQ, 5, "RobinInterruptCatcher::unconfigure: error from  pthread_kill = " << status);
      CREATE_ROS_EXCEPTION(ex6, InterruptSchedulerException, ROSINT_KILL, "error from  pthread_kill = " << status);
      throw (ex6);
    }
    
    DFThread::yield();  // let the signal do its job
    m_threadStarted = false;
    DEBUG_TEXT(DFDB_ROBINIRQ, 20, "RobinInterruptCatcher::unconfigure: interrupt catcher thread stopped");
  }

  //Return the resources
  delete m_mem_buff;
  delete m_dataPool;
  for(int card = 0; card < m_nrobins; card++)
  {
    for (int rol = 0; rol < 3; rol++)
    {
      DEBUG_TEXT(DFDB_ROBINIRQ, 20, "RobinInterruptCatcher::unconfigure: deleting ROL " << card * 3 + rol);
      delete m_rol[card * 3 + rol];
    }
    
    DEBUG_TEXT(DFDB_ROBINIRQ, 20, "RobinInterruptCatcher::unconfigure: returning resources of Robin " << card);
    m_robin[card]->freeMsgResources();
    delete m_robin[card];
    delete m_memoryPool[card];
  }

  DEBUG_TEXT(DFDB_ROBINIRQ, 15, "RobinInterruptCatcher::unconfigure: Done");
}



/****************************************/
ISInfo* RobinInterruptCatcher::getISInfo()
/****************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "RobinInterruptCatcher::getISInfo: called & done");

  return &m_statistics;
}


//Called by ROSCore/IOManager.cpp
/*************************************/
void RobinInterruptCatcher::clearInfo()  
/*************************************/
{
  DEBUG_TEXT(DFDB_ROBINIRQ, 15, "RobinInterruptCatcher::clearInfo: Entered");
  
  m_statistics.interruptsReceived = 0;
  
  DEBUG_TEXT(DFDB_ROBINIRQ, 15, "RobinInterruptCatcher::clearInfo: Done");
}


//Called by configure (see above)
/***********************************/
void RobinInterruptCatcher::run(void)
/***********************************/
{
  DEBUG_TEXT(DFDB_ROBINIRQ, 15, "RobinInterruptCatcher::run: Entered");

  T_irq_info ir_info;
  u_int ret;

  while (1) 
  {
    //As the driver (currently) handles the interrupts from all Robins at once we just use a random Robin object for waiting for the IRQ
    DEBUG_TEXT(DFDB_ROBINIRQ, 20, "RobinInterruptCatcher::run: Waiting for IRQ.....");
    ret = m_robin[0]->waitForInterrupt(&ir_info);  // no timeout. This is NOT a cancellation point.
    DEBUG_TEXT(DFDB_ROBINIRQ, 20, "RobinInterruptCatcher::run: .....IRQ (or an abort signal) received");
    if (ret)
    { 
      if (ret == RD_INTBYSIGNAL)     // signal from thread cancellation (Stop)
      {
        DEBUG_TEXT(DFDB_ROBINIRQ, 10, "RobinInterruptCatcher::run: RD_INTBYSIGNAL received. Stopping thread.");
        DFThread::stopExecution();   // the interrupt catcher will stop here ..
      }
      else 
      {
        DEBUG_TEXT(DFDB_ROBINIRQ, 5, "RobinInterruptCatcher::run: Error from waitForInterrupt. ret = 0x" << HEX(ret)); // cancellation point !
        CREATE_ROS_EXCEPTION(ex3, InterruptSchedulerException, ROSINT_IRQWAIT, "Error from waitForInterrupt, ret = " << ret);
        throw (ex3);
      }
    }

    m_statistics.interruptsReceived++;
      
    DEBUG_TEXT(DFDB_ROBINIRQ, 20, "RobinInterruptCatcher::run: Interrupt received");

    //Disable the interrups if we get too many
    //We disable all ROLs at once. Technically it would be possible to count the IRQs per ROL and to disable
    //only those ROLs that cross the MAX_ROBIN_IRQS limit.
    if (m_statistics.interruptsReceived > MAX_ROBIN_IRQS)
    {
      DEBUG_TEXT(DFDB_ROBINIRQ, 5, "RobinInterruptCatcher::run: Too many interrupts received. I will disable them now.");      
      for(int card = 0; card < m_nrobins; card++)
        for (int rol = 0; rol < 3; rol++)
          m_rol[card * 3 + rol]->setConfig(enum_cfgIrqEnable, 0);
      CREATE_ROS_EXCEPTION(ex10, InterruptSchedulerException, ROSINT_TOOMANY, "RobinInterruptCatcher::run: Too many interrupts received. I will disable interrupts from all Robin cards now.");
      ers::warning(ex10);
    }
    
    DEBUG_TEXT(DFDB_ROBINIRQ, 20, "RobinInterruptCatcher::run: ir_info.ncards = " << ir_info.ncards);      
    
    //Analyze the ir_info structure and acquire the data
    for(u_int card = 0; card < ir_info.ncards; card++)
    {
      DEBUG_TEXT(DFDB_ROBINIRQ, 20, "RobinInterruptCatcher::run: card " << card << ", channel 0: Type= " << ir_info.ch0_type[card] << ", ID = " << ir_info.ch0_id[card]);      
      DEBUG_TEXT(DFDB_ROBINIRQ, 20, "RobinInterruptCatcher::run: card " << card << ", channel 1: Type= " << ir_info.ch1_type[card] << ", ID = " << ir_info.ch1_id[card]);      
      DEBUG_TEXT(DFDB_ROBINIRQ, 20, "RobinInterruptCatcher::run: card " << card << ", channel 2: Type= " << ir_info.ch2_type[card] << ", ID = " << ir_info.ch2_id[card]);      
    
      if (ir_info.ch0_type[card] == INMATE_EVENT)  
	get_inmate_event(ir_info.ch0_id[card], card, 0);

      if (ir_info.ch1_type[card] == INMATE_EVENT)  
	get_inmate_event(ir_info.ch1_id[card], card, 1);  

      if (ir_info.ch2_type[card] == INMATE_EVENT)  
	get_inmate_event(ir_info.ch2_id[card], card, 2);  

      if (ir_info.ch0_type[card] == GUEST_EVENT)	
        get_guest_event(ir_info.ch0_id[card], card, 0);  

      if (ir_info.ch1_type[card] == GUEST_EVENT)	
        get_guest_event(ir_info.ch1_id[card], card, 1);

      if (ir_info.ch2_type[card] == GUEST_EVENT)	
        get_guest_event(ir_info.ch2_id[card], card, 2);      
    }
  }
}


/***********************************/
void RobinInterruptCatcher::cleanup()  //Called implicitly when the tread gets killed
/***********************************/
{
  DEBUG_TEXT(DFDB_ROBINIRQ, 15, "RobinInterruptCatcher::cleanup: Empty method");
}


/*********************************************************************************/
void RobinInterruptCatcher::get_inmate_event(u_int id, u_int card_no, u_int rol_no)  
/*********************************************************************************/
{
  Rol::KeptPage keptpage;
  u_int first, pid, pmrid, copy_offset = 0, page_offset = 0, logical_card;

  logical_card = m_cardmap[card_no]; 
  DEBUG_TEXT(DFDB_ROBINIRQ, 20, "RobinInterruptCatcher::get_inmate_event: phys. card = " << card_no << ", logical card = " << logical_card);

  //Reserve the full page size (at this point we do not know how many bytes will be transferred)
  u_int *buffer = (u_int *)(m_mem_buff->reserve(m_themaxevsize));
  if (buffer == 0) 
  {
    CREATE_ROS_EXCEPTION(ex1, InterruptSchedulerException, ROSINT_INVALIDPAGE, "");
    throw(ex1);
  }
  DEBUG_TEXT(DFDB_ROBINIRQ, 20, "RobinInterruptCatcher::get_inmate_event: full page reserved");
  
  first = 1;
  pid   = 0xffffffff;
  pmrid = 0xffffffff;
  while(1)  //we break out once we have read the last page of the bad event
  {
    DEBUG_TEXT(DFDB_ROBINIRQ, 20, "RobinInterruptCatcher::get_inmate_event: requesting rejected page from ROL " << logical_card * 3 + rol_no);
    DEBUG_TEXT(DFDB_ROBINIRQ, 20, "RobinInterruptCatcher::get_inmate_event: id = " << id + page_offset);

    keptpage = m_rol[logical_card * 3 + rol_no]->getRejectedPage(id + page_offset);
    if (keptpage.rentry.info.upfInfo.pageNum == 0)
    {
      DEBUG_TEXT(DFDB_ROBINIRQ, 5, "RobinInterruptCatcher::get_inmate_event: You have requested an invalid page");
      CREATE_ROS_EXCEPTION(ex4, InterruptSchedulerException, ROSINT_ILLPAGE, "You have requested an invalid page. Page number = " << id + page_offset);
      throw (ex4);
    }
    
    DEBUG_TEXT(DFDB_ROBINIRQ, 20, "RobinInterruptCatcher::get_inmate_event: Robin = " << card_no << ", Rol = " << rol_no << ", page_offset = " << page_offset);
    DEBUG_TEXT(DFDB_ROBINIRQ, 20, "RobinInterruptCatcher::get_inmate_event: Most recent L1ID                     = 0x" << HEX(keptpage.rentry.mri));
    DEBUG_TEXT(DFDB_ROBINIRQ, 20, "RobinInterruptCatcher::get_inmate_event: info.upfInfo.status                  = 0x" << HEX(keptpage.rentry.info.upfInfo.status));
    DEBUG_TEXT(DFDB_ROBINIRQ, 20, "RobinInterruptCatcher::get_inmate_event: info.upfInfo.eventId                 = 0x" << HEX(keptpage.rentry.info.upfInfo.eventId));
    DEBUG_TEXT(DFDB_ROBINIRQ, 20, "RobinInterruptCatcher::get_inmate_event: info.upfInfo.pageNum                 = 0x" << HEX(keptpage.rentry.info.upfInfo.pageNum));
    DEBUG_TEXT(DFDB_ROBINIRQ, 20, "RobinInterruptCatcher::get_inmate_event: info.upfInfo.pageLen                 = 0x" << HEX(keptpage.rentry.info.upfInfo.pageLen));
    DEBUG_TEXT(DFDB_ROBINIRQ, 20, "RobinInterruptCatcher::get_inmate_event: info.upfInfo.runNum                  = 0x" << HEX(keptpage.rentry.info.upfInfo.runNum));
    DEBUG_TEXT(DFDB_ROBINIRQ, 20, "RobinInterruptCatcher::get_inmate_event: Number of words in rejected fragment = " << keptpage.pagedata_size);
    if(first)
    {
      first = 0;
      pmrid   = keptpage.rentry.mri;
      pid = keptpage.rentry.info.upfInfo.eventId;
    }

    u_int bytes_to_copy = sizeof(RejectedEntry) + 4 + 4 * keptpage.pagedata_size;
    DEBUG_TEXT(DFDB_ROBINIRQ, 20, "RobinInterruptCatcher::get_inmate_event: Copying " << bytes_to_copy << " bytes");
    memcpy((void *)(buffer + copy_offset), (void *)&keptpage, bytes_to_copy);
    
    copy_offset += bytes_to_copy;
    page_offset++;
    
    if((keptpage.rentry.info.upfInfo.status & LAST_PAGE_MASK) == LAST_PAGE)
    {
      DEBUG_TEXT(DFDB_ROBINIRQ, 20, "RobinInterruptCatcher::get_inmate_event: This was the last page of the event");
      break;
    }
  }
  
  CREATE_ROS_EXCEPTION(ex5, InterruptSchedulerException, ROSINT_INMATE, " The L1ID COULD be 0x" << HEX(pid) 
  << " and the most recent L1ID COULD be 0x" << HEX(pmrid) 
  << ". The fragment was received on ROL " << rol_no << " of Robin " << card_no);
  ers::warning(ex5);
  
  DEBUG_TEXT(DFDB_ROBINIRQ, 20, "RobinInterruptCatcher::get_inmate_event: Releasing " << m_themaxevsize - copy_offset << " bytes");
  m_mem_buff->release(m_themaxevsize - copy_offset); 
  m_debugOut->sendData(m_mem_buff, 0);
  m_mem_buff->clear(); 
}

	
/********************************************************************************/
void RobinInterruptCatcher::get_guest_event(u_int id, u_int card_no, u_int rol_no)  
/********************************************************************************/
{
  MemoryPage *mem_page = NULL;
  u_long ticket; 
  u_int logical_card;
  ROBFragment::ROBHeader *robFragment;
  
  logical_card = m_cardmap[card_no]; 
  DEBUG_TEXT(DFDB_ROBINIRQ, 20, "RobinInterruptCatcher::get_guest_event: phys. card = " << card_no << ", logical card = " << logical_card);
  
  //Reserve the full page size (at this point we do not know how many bytes will be transferred)
  DEBUG_TEXT(DFDB_ROBINIRQ, 20, "RobinInterruptCatcher::get_guest_event: m_themaxevsize = " << m_themaxevsize);

  u_int *buffer = (u_int *)(m_mem_buff->reserve(m_themaxevsize));
  DEBUG_TEXT(DFDB_ROBINIRQ, 20, "RobinInterruptCatcher::get_guest_event: buffer = 0x" << HEX(buffer));
  if (buffer == 0) 
  {
    CREATE_ROS_EXCEPTION(ex1, InterruptSchedulerException, ROSINT_INVALIDPAGE, "");
    throw(ex1);
  }

  mem_page = m_memoryPool[logical_card]->getPage();
  DEBUG_TEXT(DFDB_ROBINIRQ, 20, "RobinInterruptCatcher::get_guest_event: mem_page = 0x" << HEX(mem_page));

  if (mem_page == 0) // Check if the allocated page is OK
  {						
    DEBUG_TEXT(DFDB_ROBINIRQ, 5, "RobinInterruptCatcher::get_guest_event: Failed to get a page from the memory pool");
    CREATE_ROS_EXCEPTION(ex4, InterruptSchedulerException, ROSINT_NOMEMPAGE, "Failed to get a page from the memory pool");
    throw (ex4);
  }

  DEBUG_TEXT(DFDB_ROBINIRQ, 20, "RobinInterruptCatcher::get_guest_event: Requesting fragment with L1ID = " << id);
  u_int *fragBufferAddress = static_cast<u_int *> (mem_page->reserve(mem_page->capacity()));
  ticket = m_rol[logical_card * 3 + rol_no]->requestFragment(id, reinterpret_cast<u_long> (fragBufferAddress));
  robFragment = (ROBFragment::ROBHeader *) m_rol[logical_card * 3 + rol_no]->getFragment(ticket);      

  DEBUG_TEXT(DFDB_ROBINIRQ, 20, "RobinInterruptCatcher::get_guest_event: Status word = 0x" << HEX(robFragment->statusElement[0]));
    
  if (robFragment->statusElement[0] & enum_fragStatusTrunc)
    { CREATE_ROS_EXCEPTION(ex5, InterruptSchedulerException, ROSINT_GUEST, "Type of error: Truncated fragment. L1ID = 0x" << HEX(id) << ". The fragment was received by ROL " << rol_no << " of Robin " << card_no); ers::warning(ex5); }
  else if (robFragment->statusElement[0] & enum_fragStatusDuplicate)
    { CREATE_ROS_EXCEPTION(ex5, InterruptSchedulerException, ROSINT_GUEST, "Type of error: Duplicated fragment. L1ID = 0x" << HEX(id) << ". The fragment was received by ROL " << rol_no << " of Robin " << card_no); ers::warning(ex5); }
  else if (robFragment->statusElement[0] & enum_fragStatusSeqError)
    { CREATE_ROS_EXCEPTION(ex5, InterruptSchedulerException, ROSINT_GUEST, "Type of error: Sequence error. L1ID = 0x" << HEX(id) << ". The fragment was received by ROL " << rol_no << " of Robin " << card_no); ers::warning(ex5); }
  else if (robFragment->statusElement[0] & enum_fragStatusSizeError)
    { CREATE_ROS_EXCEPTION(ex5, InterruptSchedulerException, ROSINT_GUEST, "Type of error: fragment size error. L1ID = 0x" << HEX(id) << ". The fragment was received by ROL " << rol_no << " of Robin " << card_no); ers::warning(ex5); }
  else if (robFragment->statusElement[0] & enum_fragStatusDataError)
    { CREATE_ROS_EXCEPTION(ex5, InterruptSchedulerException, ROSINT_GUEST, "Type of error: data block error. L1ID = 0x" << HEX(id) << ". The fragment was received by ROL " << rol_no << " of Robin " << card_no); ers::warning(ex5); }
  else if (robFragment->statusElement[0] & enum_fragStatusCtlError)
    { CREATE_ROS_EXCEPTION(ex5, InterruptSchedulerException, ROSINT_GUEST, "Type of error: ctl word error. L1ID = 0x" << HEX(id) << ". The fragment was received by ROL " << rol_no << " of Robin " << card_no); ers::warning(ex5); }
  else if (robFragment->statusElement[0] & enum_fragStatusBofError)
    { CREATE_ROS_EXCEPTION(ex5, InterruptSchedulerException, ROSINT_GUEST, "Type of error: missing BOF. L1ID = 0x" << HEX(id) << ". The fragment was received by ROL " << rol_no << " of Robin " << card_no); ers::warning(ex5); }
  else if (robFragment->statusElement[0] & enum_fragStatusEofError)
    { CREATE_ROS_EXCEPTION(ex5, InterruptSchedulerException, ROSINT_GUEST, "Type of error: missing EOF. L1ID = 0x" << HEX(id) << ". The fragment was received by ROL " << rol_no << " of Robin " << card_no); ers::warning(ex5); }
  else if (robFragment->statusElement[0] & enum_fragStatusMarkerError)
    { CREATE_ROS_EXCEPTION(ex5, InterruptSchedulerException, ROSINT_GUEST, "Type of error: invalid header marker. L1ID = 0x" << HEX(id) << ". The fragment was received by ROL " << rol_no << " of Robin " << card_no); ers::warning(ex5); }
  else if (robFragment->statusElement[0] & enum_fragStatusFormatError)
    { CREATE_ROS_EXCEPTION(ex5, InterruptSchedulerException, ROSINT_GUEST, "Type of error: Major format version mismatch. L1ID = 0x" << HEX(id) << ". The fragment was received by ROL " << rol_no << " of Robin " << card_no); ers::warning(ex5); }
  else
    { CREATE_ROS_EXCEPTION(ex5, InterruptSchedulerException, ROSINT_GUEST, "Type of error: Undefined. ROB status word = 0x " << HEX(robFragment->statusElement[0]) << " L1ID = 0x" << HEX(id) << ". The fragment was received by ROL " << rol_no << " of Robin " << card_no);  ers::warning(ex5); } 
    
  DEBUG_TEXT(DFDB_ROBINIRQ, 20, "RobinInterruptCatcher::get_guest_event: Copying " << 4 * robFragment->generic.totalFragmentsize << " bytes");
  memcpy((void *)buffer, (void *)fragBufferAddress, 4 * robFragment->generic.totalFragmentsize);  //Total fragment size (in words) is at offset 1

  mem_page->free();
  
  DEBUG_TEXT(DFDB_ROBINIRQ, 20, "RobinInterruptCatcher::get_guest_event: Releasing " << m_themaxevsize - 4 * robFragment->generic.totalFragmentsize << " bytes");
  m_mem_buff->release(m_themaxevsize - 4 * robFragment->generic.totalFragmentsize); 
  m_debugOut->sendData(m_mem_buff, 0);
  m_mem_buff->clear(); 
}	
	
	
	
