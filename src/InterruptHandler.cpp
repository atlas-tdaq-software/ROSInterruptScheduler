//$Id$
/************************************************************************/
/*									*/
/* File             : InterruptHandler.cpp				*/
/*									*/
/* Authors          : B.Gorini, M.Joos, J.Petersen CERN			*/
/*									*/
/********* C 2008 - ROS/RCD *********************************************/

#include "DFDebug/DFDebug.h"
#include "ROSUtilities/ROSErrorReporting.h"
#include "ROSInterruptScheduler/InterruptCatcher.h"
#include "ROSInterruptScheduler/InterruptHandler.h"


using namespace ROS;


/*********************************************************************/
InterruptHandler::InterruptHandler(VMEInterruptDescriptor vmeInterrupt)
/*********************************************************************/
{ 
  DEBUG_TEXT(DFDB_ROSFM, 15, "InterruptHandler::constructor: Entered");

  InterruptCatcher *pIC = InterruptCatcher::Instance();
  pIC->registerHandler(vmeInterrupt, this);
  DEBUG_TEXT(DFDB_ROSFM, 7, "InterruptHandler::constructor: register handler " << HEX(this) 
             << " in InterruptCatcher for vector = " << vmeInterrupt.getVector());

  DEBUG_TEXT(DFDB_ROSFM, 15, "InterruptHandler::constructor: Done");
}


/***********************************************************************************/
InterruptHandler::InterruptHandler(std::vector<VMEInterruptDescriptor> vmeInterrupts)
/***********************************************************************************/
{ 
  DEBUG_TEXT(DFDB_ROSFM, 15, "InterruptHandler::constructor: Entered");

  InterruptCatcher *pIC = InterruptCatcher::Instance();
  for (u_int i = 0; i < vmeInterrupts.size(); i++)
    pIC->registerHandler(vmeInterrupts[i], this);

  DEBUG_TEXT(DFDB_ROSFM, 15, "InterruptHandler::constructor: Done");
}


/***************************************/
InterruptHandler::~InterruptHandler(void)
/***************************************/
{ 
  DEBUG_TEXT(DFDB_ROSFM, 15, "InterruptHandler::destructor: Entered");

  InterruptCatcher *pIC = InterruptCatcher::Instance();
  pIC->unregisterHandler(this);

  DEBUG_TEXT(DFDB_ROSFM, 15, "InterruptHandler::destructor: Done");
}
