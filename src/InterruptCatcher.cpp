//$Id$
/************************************************************************/
/*                                                                      */
/* File             : InterruptCatcher.cpp				*/
/*                                                                      */
/* Authors          : B.Gorini, M.Joos, J.Petersen CERN                 */
/*                                                                      */
/* a singleton class for catching interrupts from VMEbus		*/
/*									*/
/********* C 2011 - ROS/RCD *********************************************/

//Unless stated otherwise all methods in this class are called from IOManager


#include <signal.h>
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"
#include "DFDebug/DFDebug.h"
#include "ROSUtilities/ROSErrorReporting.h"
#include "ROSInterruptScheduler/InterruptCatcher.h"


#define KILL_SIGNAL SIGUSR1


using namespace ROS;


//Static variables
DFFastMutex *InterruptCatcher::s_mutex = DFFastMutex::Create();
InterruptCatcher *InterruptCatcher::s_uniqueInstance = 0;
static struct sigaction m_saint, m_saold;          // For the handling of the signal that we need to stop the thread


/***********************************************************************/
static void KILL_SIGNALHandler(int sig, siginfo_t *info, void * /*data*/)
/***********************************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 10, "InterruptCatcher::KILL_SIGNALHandler, signal # = " << sig);

  if(info == NULL)
  {
    std::cout << "signal: " << sig << std::endl;
    return;
  }

  std::cout << "signal: " << sig << ", pid: " << info->si_pid << ", uid: " << info->si_uid << std::endl;
}


// Singleton pattern: only one instance is created
/********************************************/
InterruptCatcher *InterruptCatcher::Instance() 
/********************************************/
{
  s_mutex->lock();
  if (s_uniqueInstance == 0) {
    s_uniqueInstance = new InterruptCatcher;
      
    // We will start a thread. Therefore we have to install a signal handler for stopping it
    std::cout << "InterruptCatcher: Installing handler for signal " << KILL_SIGNAL << std::endl;
    memset(&m_saint, '\0', sizeof (struct sigaction));
    m_saint.sa_sigaction = &KILL_SIGNALHandler;
    m_saint.sa_flags |= SA_SIGINFO;
    sigemptyset(&m_saint.sa_mask);
    if(sigaction(KILL_SIGNAL, &m_saint, &m_saold))
    {
      perror("sigaction");
      DEBUG_TEXT(DFDB_ROSFM, 5, "InterruptCatcher::Instance: Error from sigaction");
    }
  }
  s_mutex->unlock();
  
  return s_uniqueInstance;
}


/****************************************************************************************************/
InterruptCatcher::InterruptCatcher(void) : ROSHandler("VME Interrupt catcher"), m_threadStarted(false)
/****************************************************************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "InterruptCatcher::constructor: Entered");

  // initialise and fill handler array
  for (int i = 0; i < MAXVMEINTERRUPTS; i++) 
    m_interruptHandlers.push_back(0);

  m_statistics.numberOfInterruptVectors = 0;
  m_intHandle = -1;
  DEBUG_TEXT(DFDB_ROSFM, 15, "InterruptCatcher::constructor: Done");
}


/***********************************/
InterruptCatcher::~InterruptCatcher() noexcept
/***********************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "InterruptCatcher::destructor: Entered");

  //Now the signal handler is not required any more
  if (sigaction(KILL_SIGNAL, &m_saold, (struct sigaction *)NULL))
  {
    perror("sigaction");
    DEBUG_TEXT(DFDB_ROSFM, 5, "InterruptCatcher::destructor: Error from sigaction");
  }

  s_uniqueInstance = 0;
  //We are not cleaning up completely (e.g. the sigaction is still active). This seems to have no consequences  
  DEBUG_TEXT(DFDB_ROSFM, 15, "InterruptCatcher::destructor: Done");
}


/************************************/
void InterruptCatcher::configure(const daq::rc::TransitionCmd&)
/************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "InterruptCatcher::configure: Entered. # Interrupt vectors = " << m_statistics.numberOfInterruptVectors);

  if (m_statistics.numberOfInterruptVectors == 0) // no handlers registered
    return;

  // build group interrupt link handle
  VME_InterruptList_t irq_list;
  irq_list.number_of_items = m_interrupts.size();
  for(u_int vect = 0; vect < m_interrupts.size(); vect++) 
  {
    irq_list.list_of_items[vect].vector = m_interrupts[vect].getVector();
    irq_list.list_of_items[vect].level = m_interrupts[vect].getLevel();
    irq_list.list_of_items[vect].type = m_interrupts[vect].getType();
    DEBUG_TEXT(DFDB_ROSFM, 7, "InterruptCatcher::configure: vect = " << vect
               << "  IR vector = " << (int)irq_list.list_of_items[vect].vector
               << "  IR level  = " << (int)irq_list.list_of_items[vect].level);
  }

  // What happens if m_interrupts[vect].getType() is not consistent? This is an issue for the documentation

  int intHandle;
  err_str rcc_err_str;
  err_type ret = VME_InterruptLink(&irq_list, &intHandle);   //VME_Open has been called from somewhere else...
  if (ret != VME_SUCCESS) 
  {
    DEBUG_TEXT(DFDB_ROSFM, 5, "InterruptCatcher::configure: Error from VME_InterruptLink");
    rcc_error_string(rcc_err_str, ret);
    CREATE_ROS_EXCEPTION(ex1, InterruptSchedulerException, ROSINT_VMEINTERRUPTLINK, rcc_err_str);
    throw(ex1);
  }
  DEBUG_TEXT(DFDB_ROSFM, 7, "InterruptCatcher::configure: VME_InterruptLink, handle = " << intHandle);

  m_intHandle = intHandle;
  m_statistics.interruptWaits = 0;
    
  // if there are registered handlers then start the thread
  DEBUG_TEXT(DFDB_ROSFM, 7, "InterruptCatcher::configure: going to start thread");
  m_threadStarted = true;
  startExecution();
  DEBUG_TEXT(DFDB_ROSFM, 7, "InterruptCatcher::configure: Interrupt Catcher started");
  DEBUG_TEXT(DFDB_ROSFM, 15, "InterruptCatcher::configure: Done");
}


/**************************************/
void InterruptCatcher::unconfigure(const daq::rc::TransitionCmd&)
/**************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "InterruptCatcher::unconfigure: Entered. # Interrupt vectors = " << m_statistics.numberOfInterruptVectors);

  if (m_threadStarted) 
  {
    //Stop the thread
    // 070115: just send an interrupt and the thread will stop itself
    // remember that this code executes as part of the 'main' thread - NOT the interrupt catcher thread.
    DEBUG_TEXT(DFDB_ROSFM, 7, "InterruptCatcher::unconfigure: stopping interrupt catcher thread");
    //  send an interrupt. 
    pthread_t icId = s_uniqueInstance->specificId();
    std::cout << " InterruptCatcher::unconfigure: sending signal to tid = " << HEX(icId) << std::endl;

    m_threadStarted = false;
    pthread_kill(icId, KILL_SIGNAL);

    DFThread::yield(); // let the signal do its job

    try 
    {
       waitForCondition(DFThread::TERMINATED, 1);
       std::cout << " InterruptCatcher::unconfigure: interrupt catcher thread stopped" << std::endl;
    }
    catch(DFThread::Timeout) 
    {
       CREATE_ROS_EXCEPTION(e, InterruptSchedulerException, THREAD_STOP_TIMEOUT, " VME InterruptCatcher. "
                            << ": thread is still in condition: " << getCondition());
       ers::warning(e);
    }
  }

  if (m_intHandle != -1) 
  {
    err_str rcc_err_str;
    err_type ret = VME_InterruptUnlink(m_intHandle);
    if (ret != VME_SUCCESS) 
    {
      DEBUG_TEXT(DFDB_ROSFM, 5, "InterruptCatcher::unconfigure: Error from VME_InterruptLink");
      rcc_error_string(rcc_err_str, ret);
      CREATE_ROS_EXCEPTION(ex1, InterruptSchedulerException, ROSINT_VMEINTERRUPTUNLINK, rcc_err_str);
      throw(ex1);
    }
    m_intHandle = -1;
    DEBUG_TEXT(DFDB_ROSFM, 7, "InterruptCatcher::unconfigure: Vectors Unlinked");
  }

  DEBUG_TEXT(DFDB_ROSFM, 15, "InterruptCatcher::unconfigure: Done");
}


/****************************************/
void InterruptCatcher::prepareForRun(const daq::rc::TransitionCmd&)
/****************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "InterruptCatcher::prepareForRun: Entered");
  m_statistics.interruptWaits = 0;
  DEBUG_TEXT(DFDB_ROSFM, 15, "InterruptCatcher::prepareForRun: Done");
}


/***********************************/
ISInfo *InterruptCatcher::getISInfo()
/***********************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "InterruptCatcher::getIsInfo: called");
  return &m_statistics;
  DEBUG_TEXT(DFDB_ROSFM, 15, "InterruptCatcher::getIsInfo: done");
}


/********************************/
void InterruptCatcher::clearInfo()
/********************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "InterruptCatcher::clearInfo: Entered");
  m_statistics.interruptWaits = 0;
  DEBUG_TEXT(DFDB_ROSFM, 15, "InterruptCatcher::clearInfo: Done");
}


//Called from the constructor of InterruptHandler
/*************************************************************************************************************/
void InterruptCatcher::registerHandler(VMEInterruptDescriptor vmeInterrupt, InterruptHandler *interruptHandler)
/*************************************************************************************************************/
{
  //The user code must not use the same vector twice.
  if (m_interruptHandlers[vmeInterrupt.getVector()] != 0) 
  {
    CREATE_ROS_EXCEPTION(e, InterruptSchedulerException, ROSINT_VECTORBUSY, " vector = " << vmeInterrupt.getVector());
    throw e;
  }
  else 
  {
    m_interruptHandlers[vmeInterrupt.getVector()] = interruptHandler;
    m_statistics.numberOfInterruptVectors++;
  }

  m_interrupts.push_back(vmeInterrupt);
  DEBUG_TEXT(DFDB_ROSFM, 7, "InterruptCatcher::registerHandler: handler = " << interruptHandler << " is inserted in handler vector at position " << vmeInterrupt.getVector());
}


//Called from the destructor of InterruptHandler
/**************************************************************************/
void InterruptCatcher::unregisterHandler(InterruptHandler *interruptHandler)
/**************************************************************************/
{
  bool found0 = false;
  
  for (int vector = 0; vector < MAXVMEINTERRUPTS; vector++) 
  {  
    if (m_interruptHandlers[vector] == interruptHandler) 
    {
      found0 = true;
      m_interruptHandlers[vector] = 0;
      m_statistics.numberOfInterruptVectors--;
      DEBUG_TEXT(DFDB_ROSFM, 20, "InterruptCatcher::unregisterHandler: handler = " <<
                                 interruptHandler << " is removed from handler vector at position "
                                 << vector << " # interrupt vectors in table is now "
                                 << m_statistics.numberOfInterruptVectors);
      bool found1 = false;
      for (std::vector<VMEInterruptDescriptor>::iterator it = m_interrupts.begin(); it != m_interrupts.end() && !found1; it++) 
      {
        if ((*it).getVector() == vector) 
	{
          m_interrupts.erase(it);		// iterators are no longer valid !
          found1 = true;
          DEBUG_TEXT(DFDB_ROSFM, 20, "InterruptCatcher::unregisterHandler: vector " << vector << " is erased from the group of interrupts");
        }
      }
    }
  }
  if (!found0) 
  {
    CREATE_ROS_EXCEPTION(e, InterruptSchedulerException, ROSINT_UNREGISTERNOTFOUND, "");
    throw e;
  }
}


//Called implicitely by configure()
/******************************/
void InterruptCatcher::run(void)
/******************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "InterruptCatcher::run: Entered");

  VME_InterruptInfo_t ir_info;
  err_type ret;
  err_str rcc_err_str;

  // 060410 experimentation has shown:
  // 0) a pthread_cancel alone doesn't make VMEInterruptWait return - down_interruptible not a cancellation point by itself
  // 1) if a signal is sent to the IR catcher and handled (e.g. SIGUSR1) then the VMEWAIT exits from down_interruptible
  //    and VMEInterruptWait returns to the user 
  // 2) EXCEPT if a pthread cancel is pending: then the VMEInterruptWait doesn't return AND the thread is killed
  // 3) the use of signal_pending(current) in the VMEbus driver AND in down_interruptible seems to be problematic
  //    in multi threaded configurations: it tests if a signal is pending on the current thread which may be
  //    different from the one executing the system call ...
  // 070115 change the code for SLC4.
  // remove stopExecution() in unconfigure() so as to avoid having a pending cancel when the IR is sent.
  // Instead the thread stops itself.

  while (m_threadStarted) 
  {
    DEBUG_TEXT(DFDB_ROSFM, 20, "InterruptCatcher::run: Waiting for interrupt"); // cancellation point !
    ret = VME_InterruptWait(m_intHandle, -1, &ir_info);  // no timeout. This is NOT a cancellation point.

    if (ret != VME_SUCCESS) 
    { // not likely in a multi-threaded ROS ..
      DEBUG_TEXT(DFDB_ROSFM, 5, "InterruptCatcher::run: Error from VME_InterruptWait, ret = " << HEX(ret)); // cancellation point !
      std::cout << "InterruptCatcher::run: Error from VME_InterruptWait, ret = " << std::hex <<  ret  << std::dec << std::endl;
      if (RCC_ERROR_MAJOR(ret) != VME_INTBYSIGNAL)      // signal from thread cancellation (Stop)
      {
        DEBUG_TEXT(DFDB_ROSFM, 5, "InterruptCatcher::run: VME_InterruptWait interrupted by something else than a signal"); 
        rcc_error_string(rcc_err_str, ret);
        CREATE_ROS_EXCEPTION(e, InterruptSchedulerException, ROSINT_WAIT, rcc_err_str);
        throw e;
      }
    }
    else 
    {
       DEBUG_TEXT(DFDB_ROSFM, 20, "InterruptCatcher::run: level = " << ir_info.level << " type = " << ir_info.type << " vector = " << (int)ir_info.vector);

       m_statistics.interruptWaits++;
       if (m_interruptHandlers[ir_info.vector] != 0) 
       {
          VMEInterruptDescriptor vmeInt ((int)ir_info.vector, ir_info.level, ir_info.type);
          m_interruptHandlers[ir_info.vector]->reactTo(vmeInt);
       }
       else 
       {
          CREATE_ROS_EXCEPTION(e, InterruptSchedulerException, ROSINT_NOHANDLER, " vector = " << (int)ir_info.vector);
          throw e;
       }
    }
  }
}


//Called via DFThreads when the thread is stopped
/******************************/
void InterruptCatcher::cleanup()
/******************************/
{
  std::cout << " InterruptCatcher::cleanup " << std::endl;
  DEBUG_TEXT(DFDB_ROSFM, 15, "InterruptCatcher::cleanup: Empty method");
}


