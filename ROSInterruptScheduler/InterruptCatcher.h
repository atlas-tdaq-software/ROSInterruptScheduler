//$Id$

/*
  ATLAS ROS Software

  Class: InterruptCatcher
  Author: G.Crone, M.Joos, J.Petersen, CERN
*/

#ifndef INTERRUPTCATCHER_H
#define INTERRUPTCATCHER_H

#include <signal.h>
#include "DFSubSystemItem/Config.h"
#include "ROSCore/ROSHandler.h"
#include "DFThreads/DFCountedPointer.h"
#include "DFThreads/DFFastMutex.h"
#include "ROSInfo/InterruptCatcherInfo.h"
#include "ROSInterruptScheduler/InterruptSchedulerException.h"
#include "ROSInterruptScheduler/VMEInterruptDescriptor.h"
#include "ROSInterruptScheduler/InterruptHandler.h"

namespace ROS 
{ 
  class InterruptCatcher : public ROSHandler 
  {
  public:
    static InterruptCatcher *Instance();
 
    virtual void configure(const daq::rc::TransitionCmd&);
    virtual void prepareForRun(const daq::rc::TransitionCmd&);
    virtual void unconfigure(const daq::rc::TransitionCmd&);
    virtual void clearInfo();
    virtual ISInfo* getISInfo();

    virtual ~InterruptCatcher ()noexcept;

    const std::vector<InterruptHandler *> *interruptHandlers();

    void registerHandler(VMEInterruptDescriptor vmeInterrupt, InterruptHandler *interruptHandler);
    void  unregisterHandler (InterruptHandler *interruptHandler);

  protected: 
    virtual void run();
    virtual void cleanup();

  private:
    enum 
    {
      MAXVMEINTERRUPTS = 256
    };

    static DFFastMutex *s_mutex;                            // local mutex to protect the creation of the singleton
    static InterruptCatcher *s_uniqueInstance;              // flag for the creation of the singleton
    std::vector<InterruptHandler *> m_interruptHandlers;    // array of Interrupt Handlers with fixed size 256: simpler and faster than a map
    bool m_threadStarted;                                   // indicates if a thread has been started or not
    std::vector <VMEInterruptDescriptor> m_interrupts;      // all the interrupt vectors known to the interrupt catcher (possibly corresponding to many handlers)
                                                            // they are part of one group for interupt linking (VMEInterruptLink)
    int m_intHandle;			                    // the interrupt handle of the GROUP
    InterruptCatcherInfo m_statistics;                      // statistics
    InterruptCatcher();                                     // Empty Constructor: it is private as part of the singleton pattern
  };
} 
#endif //INTERRUPTCATCHER_H

