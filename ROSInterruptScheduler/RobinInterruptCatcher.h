/************************************************************************/
/*                                                                      */
/* File             : RobinInterruptCatcher.h  				*/
/*                                                                      */
/* Author           : M.Joos, CERN                                      */
/*                                                                      */
/* a singleton class for catching interrupts from Robin cards		*/
/*									*/
/***** C 2008 Ecosoft - Made from at least 80% recycled source code *****/

#ifndef ROBININTERRUPTCATCHER_H
#define ROBININTERRUPTCATCHER_H


#include <signal.h>
#include <vector>

#include "ROSCore/ROSHandler.h"
#include "ROSRobin/robindriver_common.h"
#include "ROSRobin/Robin.h"
#include "ROSRobin/Rol.h"
#include "DFThreads/DFFastMutex.h"
#include "DFSubSystemItem/Config.h"
#include "ROSMemoryPool/WrapperMemoryPool.h"
#include "ROSMemoryPool/MemoryPool.h"
#include "ROSBufferManagement/Buffer.h"
#include "ROSCore/DataOut.h"
#include "ROSInfo/RobinInterruptCatcherInfo.h"


//Some constants
#define LAST_PAGE      0x2a000000
#define NOT_LAST_PAGE  0x55000000
#define LAST_PAGE_MASK 0x7f000000
#define MAX_ROBIN_IRQS 0x10000    //MJ: arbitrary value. Should go into the config database


namespace ROS 
{ 
  class RobinInterruptCatcher : public ROSHandler 
  {
  public:
    static RobinInterruptCatcher *Instance();
    virtual void setup(DFCountedPointer<Config> configuration);
    virtual void configure(const daq::rc::TransitionCmd&);
    virtual void unconfigure(const daq::rc::TransitionCmd&);
    virtual void clearInfo();
    virtual ISInfo* getISInfo(void);
    virtual ~RobinInterruptCatcher() noexcept;

    void deregister(DFCountedPointer<Config> configuration);
  protected: 
    virtual void run();
    virtual void cleanup();

  private:
    RobinInterruptCatcher();                           //Empty Constructor: it is private as part of the singleton pattern
    void get_guest_event(u_int id, u_int card_no, u_int rol_no);  
    void get_inmate_event(u_int id, u_int card_no, u_int rol_no);  
    RobinInterruptCatcherInfo m_statistics;
    u_int m_themaxevsize;                              // The max size of a corrupted event
    static DFFastMutex *s_mutex;                       // Mutex to protect s_uniqueInstance
    static RobinInterruptCatcher *s_uniqueInstance;    // Flag for the creation of the singleton
    bool m_threadStarted;                              // Indicates if a thread has been started or not    
    int m_intHandle;			               // The interrupt handle of the GROUP (as returned bny vme_rcc)
    int m_nrobins;                                     // The number of Robin cards in the PC
    std::vector<DFCountedPointer<Config> > m_configuration;
    Robin *m_robin[MAXCARDS];                          // An array of pointers to the Robin objects controlling the Robin hardware
    Rol *m_rol[3 * MAXCARDS];                          // An array of pointers to the Rol objects 
    u_int m_cardmap[MAXCARDS];                         // This array maps logical card numbers onto physical addresses
    WrapperMemoryPool *m_memoryPool[MAXCARDS];         // The memory pools of the Robins
    MemoryPool *m_dataPool;                            // The memory pool for the data of the corrupted events
    Buffer *m_mem_buff;                                // The memory buffer for the data of the corrupted events
    DataOut *m_debugOut;                               // The object for sending the data out to the external world
  };
} 
#endif 

