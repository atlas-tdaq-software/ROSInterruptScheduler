// $Id$

/*
  ATLAS TDAQ ROS/RCD

  Class: InterruptHandler
  Authors: B.Gorini, M.Joos, J.Petersen

*/

#ifndef INTERRUPTHANDLER_H
#define INTERRUPTHANDLER_H

#include <vector>
#include "ROSInterruptScheduler/VMEInterruptDescriptor.h"

namespace ROS 
{
  class InterruptHandler 
  {
  public:    
    InterruptHandler(VMEInterruptDescriptor vmeInterrupt);
    InterruptHandler(std::vector<VMEInterruptDescriptor> vmeInterrupts);
    virtual ~InterruptHandler();
    virtual void reactTo(VMEInterruptDescriptor vmeInterrupt) = 0; 
  };
}
#endif //INTERRUPTHANDLER_H
