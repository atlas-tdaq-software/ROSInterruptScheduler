//$Id$

/*
  ATLAS ROS Software

  Class: VMEInterruptDescriptor
  Author: B.Gorini, J.Petersen CERN 	
*/

#ifndef VMEINTERRUPTDESCRIPTOR_H
#define VMEINTERRUPTDESCRIPTOR_H

#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"

namespace ROS 
{
  class VMEInterruptDescriptor
  {
  public:
    VMEInterruptDescriptor(int vector, int level = 3, int type = ROAK);
    ~VMEInterruptDescriptor() {};

    int getVector(void);
    int getLevel(void);
    int getType(void);

  private:
    enum Type 
    {
      ROAK = VME_INT_ROAK,
      RORA = VME_INT_RORA
    };

    int m_vector;
    int m_level;
    int m_type;
  };

  inline VMEInterruptDescriptor::VMEInterruptDescriptor(int vector, int level, int type) : m_vector(vector), m_level(level), m_type(type)
  {
  }

  inline int VMEInterruptDescriptor::getVector(void) 
  {
    return m_vector;
  }

  inline int VMEInterruptDescriptor::getLevel(void) 
  {
    return m_level;
  }

  inline int VMEInterruptDescriptor::getType(void)
  {
    return m_type;
  }

}
#endif // VMEINTERRUPTDESCRIPTOR_H
