// -*- c++ -*-
/*
  Atlas Ros Software

  Class: INTERRUPTSCHEDULEREXCEPTION
  Authors: ATLAS ROS group 	
*/

#ifndef INTERRUPTSCHEDULEREXCEPTION_H
#define INTERRUPTSCHEDULEREXCEPTION_H

#include <sys/types.h>
#include "DFExceptions/ROSException.h"

namespace ROS 
{
  class InterruptSchedulerException : public ROSException 
  {    
  public:
    enum ErrorCode 
    { 
      POLL_TSCLOCK,
      ROSINT_VMEINTERRUPTLINK,
      ROSINT_VMEINTERRUPTUNLINK,
      ROSINT_UNREGISTERNOTFOUND,
      ROSINT_WAIT,
      ROSINT_NOHANDLER, 
      ROSINT_VECTORBUSY, 
      ROSINT_MALLOC, 
      ROSINT_KILL,
      ROSINT_ILLPAGE, 
      ROSINT_NOMEMPAGE,
      ROSINT_INVALIDPAGE,
      ROSINT_DATAOUT,
      ROSINT_TOOMANY,
      THREAD_STOP_TIMEOUT,
      ROSINT_INMATE,
      ROSINT_IRQWAIT,
      ROSINT_GUEST
    }; 

    InterruptSchedulerException(ErrorCode error);
    InterruptSchedulerException(ErrorCode error, std::string description);
    InterruptSchedulerException(ErrorCode error, const ers::Context& context);
    InterruptSchedulerException(ErrorCode error, std::string description, const ers::Context& context);
    InterruptSchedulerException(const std::exception& cause, ErrorCode error, std::string description, const ers::Context& context);
    virtual ~InterruptSchedulerException() throw () {};

  protected:
    virtual std::string getErrorString(u_int errorId) const;

    virtual ers::Issue * clone() const { return new InterruptSchedulerException( *this ); }
    static const char * get_uid() {return "ROS::InterruptSchedulerException";}
    virtual const char* get_class_name() const {return get_uid();}
  };
  
  inline InterruptSchedulerException::InterruptSchedulerException(InterruptSchedulerException::ErrorCode error) 
     : ROSException("ROSInterruptScheduler", error, getErrorString(error)) { }
   
  inline InterruptSchedulerException::InterruptSchedulerException(InterruptSchedulerException::ErrorCode error, std::string description) 
     : ROSException("ROSInterruptScheduler", error, getErrorString(error), description) { }

  inline InterruptSchedulerException::InterruptSchedulerException(InterruptSchedulerException::ErrorCode error, const ers::Context& context)
     : ROSException("ROSInterruptScheduler", error, getErrorString(error), context) { }
   
  inline InterruptSchedulerException::InterruptSchedulerException(InterruptSchedulerException::ErrorCode error, std::string description, const ers::Context& context) 
     : ROSException("ROSInterruptScheduler", error, getErrorString(error), description, context) { }

  inline InterruptSchedulerException::InterruptSchedulerException(const std::exception& cause, ErrorCode error, std::string description, const ers::Context& context) 
     : ROSException(cause, "ROSInterruptScheduler", error, getErrorString(error), description, context) { }

  inline std::string InterruptSchedulerException::getErrorString(u_int errorId) const 
  {
    std::string rc;    
    switch (errorId) 
    {  
      case POLL_TSCLOCK:	      rc = "Error in ts_clock"; 									break;
      case ROSINT_VMEINTERRUPTLINK:   rc = "Error in VME_InterruptLink";								break;
      case ROSINT_VMEINTERRUPTUNLINK: rc = "Error in VME_InterruptUnlink";								break;
      case ROSINT_UNREGISTERNOTFOUND: rc = "Handler not found";										break;
      case ROSINT_WAIT:	              rc = "Error in VMEInterruptWait";									break;
      case ROSINT_NOHANDLER:	      rc = "No handler for this vector";								break;
      case ROSINT_VECTORBUSY:	      rc = "Already a handler for this vector";								break;
      case ROSINT_MALLOC:      	      rc = "Error from call to malloc()";								break;
      case ROSINT_KILL:      	      rc = "Error from call to pthread_kill()";								break;      
      case ROSINT_ILLPAGE:	      rc = "You have requested an illegal page from the jail of a Robin";				break;
      case ROSINT_NOMEMPAGE:          rc = "Failed to get a page from the memory pool";							break;  
      case ROSINT_INVALIDPAGE:        rc = "Error from call to Buffer::reserve()";							break;  
      case ROSINT_DATAOUT:            rc = "Did not manage to create a dataOut object";							break;
      case ROSINT_TOOMANY:  	      rc = "Too many interrupts";									break;
      case THREAD_STOP_TIMEOUT:       rc = "TimeOut on stopping thread";								break;  
      case ROSINT_IRQWAIT:	      rc = "Error in Robin::waitForInterrupt()";							break;
      case ROSINT_INMATE:	      rc = "Badly corrupted event fragment received.";							break;      
      case ROSINT_GUEST:	      rc = "Error in event fragment. Please retrieve the fragment via the ED and analyze it carefully.";break;      
      default:	                      rc = "Unknown error";										break;
    }
    return rc;
  }
}
#endif //INTERRUPTSCHEDULEREXCEPTION_H
